package dat251.dependencyinjection;

public class CarRace {
	
	private Car firstCar;
	private Car secondCar;

	public void setFirstCar(Car car) {
		firstCar = car;
	}		
	
	public void setSecondCar(Car car) {
		secondCar = car;
	}		
	
	public void run() {
		System.out.println("First car in the race: " + firstCar);
		System.out.println("Second car in the race: " + secondCar);
	}
	
}