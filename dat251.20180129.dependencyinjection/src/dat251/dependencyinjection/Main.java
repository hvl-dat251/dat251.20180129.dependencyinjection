package dat251.dependencyinjection;

public class Main {
	
	public static void main(String[] args) {
		
		CarFactory factory = new CarFactory();
		CarRace race = new CarRace();
		
		race.setFirstCar(factory.createFirstCar());
		race.setSecondCar(factory.createSecondCar());
		
		race.run();

	}
	
}